package com.devcamp.s50.jbr5_40;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr540Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr540Application.class, args);
	}

}
