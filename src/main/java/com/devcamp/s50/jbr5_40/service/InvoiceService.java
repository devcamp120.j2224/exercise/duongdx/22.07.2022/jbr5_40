package com.devcamp.s50.jbr5_40.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.devcamp.s50.jbr5_40.model.Invoice;

@Service
public class InvoiceService {
    @Autowired
    private CustomerService customerService ;
    Invoice HoaDon1 = new Invoice(1, 20000);
    Invoice HoaDon2 = new Invoice(2, 32000);
    Invoice HoaDon3 = new Invoice(3, 14000);

   
    public ArrayList<Invoice> getAllHoaDon(){
        ArrayList<Invoice> AllHoaDon = new ArrayList<>();

        HoaDon1.setCustomer(customerService.customer1);
        HoaDon2.setCustomer(customerService.customer2);
        HoaDon3.setCustomer(customerService.customer3);


        AllHoaDon.add(HoaDon1);
        AllHoaDon.add(HoaDon2);
        AllHoaDon.add(HoaDon3);
        
        return AllHoaDon ;
    }

}
