package com.devcamp.s50.jbr5_40.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.jbr5_40.model.Invoice;
import com.devcamp.s50.jbr5_40.service.InvoiceService;

@RequestMapping("/")
@RestController
@CrossOrigin()
public class InvoiceController {
    @Autowired
    private InvoiceService InvoiceService;
    @GetMapping("/invoices")
    public ArrayList<Invoice> getInvoice(){
        ArrayList<Invoice> DanhSachHoaDon = InvoiceService.getAllHoaDon();
        return DanhSachHoaDon ;
    }
}
