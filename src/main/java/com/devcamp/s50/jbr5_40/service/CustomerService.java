package com.devcamp.s50.jbr5_40.service;

import java.util.ArrayList;


import org.springframework.stereotype.Service;
import com.devcamp.s50.jbr5_40.model.Customer;

@Service
public class CustomerService {
   

    Customer customer1 = new Customer(1, "Duong", 15);
    Customer customer2= new Customer(2, "Linh", 20);
    Customer customer3 = new Customer(3, "Dao", 25);

    public ArrayList<Customer> getCustomer(){
        ArrayList<Customer> customersAll = new ArrayList<>();
        customersAll.add(customer1);
        customersAll.add(customer2);
        customersAll.add(customer3);

        return customersAll ;
    }
}
