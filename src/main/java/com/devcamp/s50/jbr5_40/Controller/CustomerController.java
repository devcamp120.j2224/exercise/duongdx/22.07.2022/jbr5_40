package com.devcamp.s50.jbr5_40.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.jbr5_40.model.Customer;
import com.devcamp.s50.jbr5_40.service.CustomerService;

@RequestMapping("/")
@RestController
@CrossOrigin()
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @GetMapping("/customers")
    public ArrayList<Customer> getCustomer(){
        ArrayList<Customer> DanhSachKhacHang = customerService.getCustomer();
        return DanhSachKhacHang ;
    }
}
